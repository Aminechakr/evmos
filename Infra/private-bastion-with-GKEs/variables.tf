/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "cluster1_name_suffix" {
  description = "A suffix to append to the default cluster name"
  default     = ""
}

variable "cluster2_name_suffix" {
  description = "A suffix to append to the default cluster name"
  default     = ""
}

variable "region" {
  description = "The region to host the cluster in"
}

variable "network" {
  description = "The VPC network to host the cluster in"
}

variable "subnetwork1" {
  description = "The subnetwork to host the cluster in"
}

variable "subnetwork2" {
  description = "The subnetwork to host the cluster in"
}

variable "subnet_ip1" {
  description = "The subnetwork to host the cluster in"
}

variable "subnet_ip2" {
  description = "The subnetwork to host the cluster in"
}

variable "ip_range_pods_name1" {
  description = "The secondary ip range to use for pods"
}

variable "ip_range_services_name1" {
  description = "The secondary ip range to use for services"
}

variable "ip_range_pods_name2" {
  description = "The secondary ip range to use for pods"
}

variable "ip_range_services_name2" {
  description = "The secondary ip range to use for services"
}



variable "ip_range_pods_value1" {
  description = "The secondary ip range to use for pods"
}

variable "ip_range_services_value1" {
  description = "The secondary ip range to use for services"
}

variable "ip_range_pods_value2" {
  description = "The secondary ip range to use for pods"
}

variable "ip_range_services_value2" {
  description = "The secondary ip range to use for services"
}

variable "compute_engine_service_account" {
  description = "Service account to associate to the nodes in the cluster"
}

variable "bastion_members" {
  type        = list(string)
  description = "List of users, groups, SAs who need access to the bastion host"
  default     = []
}
