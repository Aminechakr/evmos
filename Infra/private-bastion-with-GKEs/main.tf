/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  cluster_type = "simple-regional-private"
}

data "google_client_config" "default" {}

provider "google" {
  project     = var.project_id
  region      = var.region
}

provider "kubernetes" {
  host                   = "https://${module.gke-01.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke-01.ca_certificate)
}

//data "google_compute_subnetwork" "subnetwork" {
//  name    = var.subnetwork
//  project = var.project_id
//  region  = var.region
//}


##################
module "gke-01" {
  source                    = "../gke/modules/private-cluster/"
  project_id                = var.project_id
  name                      = "${local.cluster_type}-cluster${var.cluster1_name_suffix}"
  regional                  = true
  region                    = var.region
  network                   = module.vpc.network_name
  subnetwork                = module.vpc.subnets_names[0]
  ip_range_pods             = module.vpc.subnets_secondary_ranges[0][0].range_name
  ip_range_services         = module.vpc.subnets_secondary_ranges[0][1].range_name
  create_service_account    = false
  service_account           = var.compute_engine_service_account
  enable_private_endpoint   = true
  enable_private_nodes      = true
  master_ipv4_cidr_block    = "172.16.0.0/28"
  default_max_pods_per_node = 20
  remove_default_node_pool  = true
  database_encryption = [
    {
      "key_name" : module.kms.keys["gke-evmos-dev-key-001"],
      "state" : "ENCRYPTED"
    }
  ]

  node_pools = [
    {
      name              = "pool-01"
      min_count         = 1
      max_count         = 2
      local_ssd_count   = 0
      disk_size_gb      = 20
      disk_type         = "pd-standard"
      auto_repair       = true
      auto_upgrade      = true
      service_account   = var.compute_engine_service_account
      preemptible       = false
      max_pods_per_node = 50
    },
  ]

  master_authorized_networks = [
    {
      cidr_block   = var.subnet_ip1
      display_name = "VPC"
    },
  ]
}

##################
module "gke-02" {
  source                    = "../gke/modules/private-cluster/"
  project_id                = var.project_id
  name                      = "${local.cluster_type}-cluster${var.cluster2_name_suffix}"
  regional                  = true
  region                    = var.region
  network                   = module.vpc.network_name
  subnetwork                = module.vpc.subnets_names[1]
  ip_range_pods             = module.vpc.subnets_secondary_ranges[1][0].range_name
  ip_range_services         = module.vpc.subnets_secondary_ranges[1][1].range_name
  create_service_account    = false
  service_account           = var.compute_engine_service_account
  enable_private_endpoint   = true
  enable_private_nodes      = true
  master_ipv4_cidr_block    ="172.16.1.0/28"
  default_max_pods_per_node = 20
  remove_default_node_pool  = true
  database_encryption = [
    {
      "key_name" : module.kms.keys["gke-evmos-dev-key-001"],
      "state" : "ENCRYPTED"
    }
  ]
  node_pools = [
    {
      name              = "pool-02"
      min_count         = 1
      max_count         = 2
      local_ssd_count   = 0
      disk_size_gb      = 20
      disk_type         = "pd-standard"
      auto_repair       = true
      auto_upgrade      = true
      service_account   = var.compute_engine_service_account
      preemptible       = false
      max_pods_per_node = 50
    },
  ]

  master_authorized_networks = [
    {
      cidr_block   = var.subnet_ip2
      display_name = "VPC"
    },
  ]
}
