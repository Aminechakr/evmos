/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = ">= 4.0.1"

  project_id   = module.enabled_google_apis.project_id
  network_name = var.network
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name           = var.subnetwork1
      subnet_ip             = var.subnet_ip1
      subnet_region         = var.region
      subnet_private_access = true
      description           = "subnet #1 is managed by Terraform"
    },
    {
      subnet_name           = var.subnetwork2
      subnet_ip             = var.subnet_ip2
      subnet_region         = var.region
      subnet_private_access = true
      description           = "subnet #1 is managed by Terraform"
    }
  ]
  secondary_ranges = {
    (var.subnetwork1) = [
      {
        range_name    = var.ip_range_pods_name1
        ip_cidr_range = var.ip_range_pods_value1
      },
      {
        range_name    = var.ip_range_services_name1
        ip_cidr_range = var.ip_range_services_value1
      },
    ],
    (var.subnetwork2) = [
      {
        range_name    = var.ip_range_pods_name2
        ip_cidr_range = var.ip_range_pods_value2
      },
      {
        range_name    = var.ip_range_services_name2
        ip_cidr_range = var.ip_range_services_value2
      },
    ]
  }
}


module "cloud-nat" {
  source        = "terraform-google-modules/cloud-nat/google"
  version       = "~> 4.0"
  project_id    = module.enabled_google_apis.project_id
  region        = var.region
  router        = "evmos-router"
  network       = module.vpc.network_self_link
  create_router = true
}
