## Table of Contents

- [🚀 Solution Overview](#🚀-solution-overview)
- [🎯 Objectives](#🎯-objectives)
- [🔑 Importance](#🔑-importance)
- [🛠️ Tools](#🛠️-tools)
- [🌟 Getting Started](#🌟-getting-started)
  - [🔧 Prerequisites](#🔧-prerequisites)
  - [🏁 Setting up the Environment](#🏁-setting-up-the-environment)
- [💡 Usage](#💡-usage)
  - [⚙️ Deploying the GKE clusters](#⚙️-deploying-the-gke-clusters)
  - [⚙️ Installing ArgoCD](#⚙️-deploying-argocd)
  - [⚙️ Installing CertManager](#⚙️-installing-certmanager)
  - [⚙️ Configuring the API Gateway](#⚙️-configuring-the-api-gateway)
  - [⚙️ Installing NginX Ingress (alternative)](#⚙️-installing-nginx-ingress-alternative)
  - [⚙️ Deploying Grafana and Prometheus](#⚙️-deploying-grafana-and-prometheus)
  - [⚙️ Deploying Evmos](#⚙️-deploying-evmos)
- [🔄 Continuous Integration](#🔄-continuous-integration)
  - [📦 Stages](#📦-stages)
  - [🧪 Test Stage](#🧪-test-stage)
  - [🔍 Scan Stage](#🔍-scan-stage)
  - [🚢 Release Stage](#🚢-release-stage)
- [🔍 Troubleshooting](#troubleshooting)
- [📚 References](#references)

## 🚀 Solution Overview

This project is focused on **Automating** the setup and management of a GKE multi-cluster environment for **EVMos**, a framework that allows Ethereum Virtual Machine (**EVM**) execution on the Cosmos blockchain. By achieving the **automation** of various tasks and the implementation of best practices, this project aims to enhance the **efficiency**, **reliability**, and **scalability** of the EVMos environment using a single SCP.

Here's a brief overview of the workflow:

![Architecture of the solution](./img/architecture.png)

- Your project begins with the creation of GKE clusters using Terraform. These clusters serve as the foundational infrastructure for hosting containerized applications, including EVMos.

- ArgoCD, following a GitOps approach, continuously monitors the designated Git repositories for changes. These repositories contain application configurations and manifests.

- When changes are detected in the Git repositories, ArgoCD takes action. It automatically deploys the updated configurations to the appropriate GKE clusters. This process ensures that the environment stays in sync with the desired state.

- Grafana and Prometheus work hand in hand to provide real-time monitoring and observability. Grafana offers interactive visualization of critical metrics, while Prometheus collects and stores time-series data to provide accurate insights into system operations.

- GitLab's CI/CD pipeline automates testing and deployment processes, ensuring the reliability and integrity of applications. This automation reduces the likelihood of errors and accelerates the delivery of updates and new features.

- K9S simplifies the interaction with Kubernetes clusters, making it easier to manage resources and monitor the environment.

This collaborative workflow streamlines the setup and management of the EVMos environment, reducing manual tasks, enhancing security, and ensuring an efficient, scalable, and well-monitored infrastructure for the Cosmos blockchain.

## 🎯 Objectives

The primary **objectives** of this project include:

- **Automating** the creation of Google Kubernetes Engine (GKE) clusters on Google Cloud Platform (**GCP**).
- Deploying **ArgoCD** to facilitate GitOps continuous delivery.
- Establishing a private **GKE cluster** to enhance **security** and **flexibility**.
- Implementing **multi-cluster management** with ArgoCD for seamless application deployment.
- Setting up **monitoring** using **Grafana** and **Prometheus** to ensure system health.
- Deploying the custom application, **evmosd**, and **monitoring** it for **reliability**.
- Creating a comprehensive **Continuous Integration (CI) pipeline** to **automate** the entire process.
- Enabling **public access** to **evmosd** via a **Kubernetes gateway controller**, emphasizing **security** and efficient request routing.

## 🔑 Importance

- It streamlines and simplifies the setup and management of EVMos environments, reducing manual tasks and potential errors.
- It ensures the **security** and **reliability** of the ecosystem by incorporating best practices.

## 🛠️ Tools

We utilize a set of essential tools and technologies, each with a specific role to enhance the automation and management of the environment:

1. **Google Kubernetes Engine (GKE):** GKE is a managed Kubernetes service provided by Google Cloud Platform (GCP). It plays a pivotal role in hosting our containerized applications. GKE offers a scalable and reliable infrastructure, ensuring that our applications run efficiently on Google Cloud.

2. **GitLab (Continuous Integration and Continuous Delivery - CI/CD):** GitLab is the linchpin for establishing a comprehensive CI/CD pipeline. It automates the deployment process, ensuring that changes to our environment are thoroughly tested and seamlessly integrated. This automation reduces the likelihood of errors and accelerates the delivery of updates and new features.

3. **ArgoCD (GitOps Continuous Delivery):** ArgoCD serves as the central piece for GitOps continuous delivery. It automates the deployment of applications and configurations, ensuring that our multi-cluster environment stays in sync with our desired state. ArgoCD simplifies and streamlines the process of deploying and managing applications across Google Kubernetes Engine (GKE) clusters.

4. **Grafana and Prometheus (Monitoring and Observability):** Grafana and Prometheus work in tandem to provide monitoring and observability for your project. Grafana offers real-time analytics and interactive visualization, enabling us to monitor the health and performance of ArgoCD and other vital components. Prometheus collects and stores time-series data, ensuring that we have accurate insights into our system's operation. Together, these tools help us maintain a reliable and efficient environment.

5. **Terraform:** Terraform is a fundamental part of our infrastructure-as-code (IaC) approach. It enables us to script and manage the creation and configuration of various resources in a consistent and automated manner, ensuring that our environment is reliable and reproducible.

## 🌟 Getting Started

To get started with this project, refer to the documentation and references provided in this README. It will guide you through the setup and utilization of the **automated** EVMos environment.

We invite you to explore your project's tools, resources, and references to gain a comprehensive understanding of how EVMos **automation** can benefit your blockchain and decentralized application deployment endeavors.

### 🔧 Prerequisites

To effectively work with the EVMos Automation Project and manage the environment, you'll need the following essential tools installed:

1. **[tfenv (Terraform Environment Manager)](https://github.com/tfutils/tfenv):** tfenv is a version manager for Terraform, enabling you to work with different Terraform versions seamlessly. It's essential for maintaining consistency and compatibility within your infrastructure-as-code setup.

2. **[kubectl (Kubernetes Command-Line Tool)](https://kubernetes.io/docs/tasks/tools/):** kubectl is the command-line tool for interacting with Kubernetes clusters. It's crucial for managing containers, applications, and resources within your Kubernetes environment.

3. **[K9S (Kubernetes CLI To Manage Your Clusters In Style)](https://k9ss.io/):** K9S is a powerful CLI tool for managing Kubernetes clusters. It provides a stylish and efficient way to interact with your Kubernetes resources, making cluster management a breeze.

4. **[Google Cloud CLI (gcloud)](https://cloud.google.com/sdk/gcloud):** The Google Cloud CLI, commonly known as gcloud, is crucial for managing your Google Kubernetes Engine (GKE) clusters on Google Cloud Platform. It provides the necessary commands to interact with your GKE resources and infrastructure.

5. **[Helm](https://helm.sh/docs/intro/install/):** Helm is a package manager for Kubernetes that simplifies the deployment of applications and services. You'll use Helm to streamline the installation and management of components within your Kubernetes clusters.

Before you begin working on your project, make sure you have these tools installed and configured on your development environment. Properly setting up these tools will facilitate the management, deployment, and monitoring of your EVMos environment.

### 🏁 Setting up the environment

To begin working with the EVMos Automation Project, you can execute the following commands to set up your environment using the Google Cloud SDK (gcloud) CLI:

```bash
# List the currently authenticated accounts
gcloud auth list

# Log in with your application default credentials
gcloud auth application-default login

# Set your active Google Cloud account (replace <your_account_id> with your Google Cloud account email)
gcloud config set account <your_account_id>

# List your available Google Cloud projects
gcloud projects list

# Display the current configuration settings
gcloud config list

# Set your project you want to work with
gcloud config set project evmos-dev

# List the available compute networks in your project
gcloud compute networks list
```

## 💡 Usage

### ⚙️ Deploying the GKE clusters

To apply the infrastructure for your EVMos Automation Project, follow these Terraform setup steps:

```bash
# Install and use a specific Terraform version (1.3.0 in this example)
tfenv install 1.3.0
tfenv use 1.3.0

# Initialize the Terraform configuration
terraform init
```

Create a `.tfvars` file for your development environment, for example, `dev.tfvars`.
This file will contain the variables specific to your setup. Populate it with the necessary configuration values.

```hcl
# Define project ID for the Google Cloud project
project_id = "evmos-dev"

# Specify the Google Cloud region
region = "europe-west10"

# Define the network name for your project
network = "evmos-cluster-dev-network"

# Cluster names
cluster1_name_suffix = "evmos-01"
cluster2_name_suffix = "evmos-02"

# Subnets and their IP ranges
subnetwork1 = "evmos-cluster-dev-subnet-01"
subnet_ip1 = "10.1.0.0/16"

subnetwork2 = "evmos-cluster-dev-subnet-02"
subnet_ip2 = "10.2.0.0/16"

# Define IP ranges for pods and services in the first subnet
ip_range_pods_name1 = "ip-range-dev-pods-1"
ip_range_services_name1 = "ip-range-dev-svc-1"
ip_range_pods_value1 = "192.168.0.0/18"
ip_range_services_value1 = "192.168.64.0/18"

# Define IP ranges for pods and services in the second subnet
ip_range_pods_value2 = "192.168.128.0/18"
ip_range_services_value2 = "192.168.192.0/18"
ip_range_pods_name2 = "ip-range-dev-pods-1"
ip_range_services_name2 = "ip-range-dev-svc-2"

# Define the service account for Compute Engine
compute_engine_service_account = "bastion@evmos-dev.iam.gserviceaccount.com"

# Specify members for the bastion service account
bastion_members = [
    "serviceAccount:bastion@evmos-dev.iam.gserviceaccount.com"
]
```

Now, run the following commands:

```bash
# Generate a Terraform plan for the infrastructure
terraform plan -var-file="dev.tfvars"

# Apply the Terraform configuration to create the specified infrastructure
terraform apply -var-file="dev.tfvars"
```

These commands will automatically do the following:

1. Create a VPC network with 2 private subnets. The CIDR block of each subnet does not overlap with each other.
2. Create two GKE clusters, and each cluster uses one of the subnets created. These clusters will host the EVMOS blockchain nodes.
   - A Cloud DNS in VPC scope is used so that the cluster can parse the Pod and Service addresses in other clusters.
   - Dedicated node pools used by EVMOS will be created for each cluster.
3. Configure and update the firewall rules.
4. Verify the network interconnectivity before deployment.
5. Deploy the GKE clusters.

### ⚙️ Installing ArgoCD

To deploy ArgoCD and facilitate GitOps continuous delivery in your EVMos environment, run these commands:

1. ArgoCD deployment

```bash
# Add the ArgoCD Helm repository with the specified URL
helm repo add argo-cd https://argoproj.github.io/argo-helm

# Update the Helm repositories to ensure you have the latest repository information
helm repo update

# Install or upgrade the 'argocd' chart, creating the 'argocd' namespace if it doesn't exist.
# It uses configuration files 'argocd/values.yaml' and 'argocd/dev.yaml' for installation.
helm upgrade --install argocd argocd -f argocd/values.yaml -f argocd/dev.yaml --create-namespace -n argocd
```
2. ArgoCD configuration
  - Make sure an ApplicationSet is deployed
  - Create your project
  - Grant access users to your project, using a SAML or Oauth2 solution integrate better with external ADs, where specefic boundaries can be set for IAM.
  - Connect your git repositories


3. Connect multiple clusters on ArgoCD
  - Make sure your context is set to a different cluster with : ``kubectl config set-context gke_evmos-dev_europe-west10_simple-regional-private-clusterevmos-02``
  - argocd cluster list with ``argocd cluster list``
  - argocd clsuter add with ``argocd cluster add gke_evmos-dev_europe-west10_simple-regional-private-clusterevmos-02``
  - check the stdout and check also in ArgoCD UI in clusters

### ⚙️ Installing CertManager

CertManager is a Kubernetes native certificate management controller. These commands install CertManager on your GKE clusters.

```bash
# Add Jetstack Helm repository to access Cert-Manager
helm repo add jetstack https://charts.jetstack.io

# Update Helm repositories to ensure you have the latest charts
helm repo update

# Install Cert-Manager using Helm, specifying values from 'cert-manager/values.yaml' and 'cert-manager/dev.yaml' for configuration
helm upgrade --install cert-manager cert-manager -f cert-manager/values.yaml -f cert-manager/dev.yaml --create-namespace -n cert-manager

# Create Custom Resource Definitions (CRDs) for CertificateRequests
# Some CRD files sometimes have issues to be applied. As an alternative, we manually apply the create subcommand to have the configuration applied on the cluster
kubectl create -f https://raw.githubusercontent.com/cert-manager/cert-manager/master/deploy/crds/crd-certificaterequests.yaml
```

### ⚙️ Configuring the API Gateway

The Gateway API is a Kubernetes API for HTTP and TCP routing. These commands configure the Gateway API on your clusters:

```bash
# Clone the 'gateway-api' repository from the specified GitHub URL
git clone https://github.com/kubernetes-sigs/gateway-api.git

# Change the current directory to 'gateway-api/config/crd/'
cd gateway-api/config/crd/

# Apply Kubernetes resource configurations from the current directory
kubectl apply -f .

# Create a Kubernetes YAML file 'gateway-class.yml' and specify its content
cat <<EOF > gateway-class.yml
---
apiVersion: v1
kind: Namespace
metadata:
  name: infra
  labels:
    shared-gateway-access: "true"

---
kind: Gateway
apiVersion: gateway.networking.k8s.io/v1beta1
metadata:
  name: external-http
  namespace: infra
spec:
  gatewayClassName: gke-l7-global-external-managed
  listeners:
  - name: http
    protocol: HTTP
    port: 80
    allowedRoutes:
      kinds:
      - kind: HTTPRoute
      namespaces:
        from: Selector
        selector:
          matchLabels:
            shared-gateway-access: "true"
EOF

# Get Custom Resource Definitions (CRDs) related to 'gateway'
kubectl get crds | grep gateway

# Change the current directory to 'gateway-api/config/crd/experimental/'
cd gateway-api/config/crd/experimental/

# Apply Kubernetes resource configurations from the current directory
kubectl apply -f .

# Get Custom Resource Definitions (CRDs) related to 'gateway' again
kubectl get crds | grep gateway

# Update a Google Cloud Container Cluster named 'simple-regional-private-clusterevmos-01'
# with the specified 'gateway-api' setting and location
gcloud container clusters update simple-regional-private-clusterevmos-01 --gateway-api=standard --location=europe-west10
```

### ⚙️ Installing NginX Ingress (alternative)

Nginx Ingress is a Kubernetes ingress controller for managing external network traffic. This is how to install Nginx Ingress on your GKE clusters:

```bash
# Add the nginx-stable Helm repository
helm repo add nginx-stable https://helm.nginx.com/stable

# Update the Helm repositories
helm repo update

# Install the ingress-nginx using Helm
helm upgrade --install ingress-nginx ingress-nginx -f ingress-nginx/values.yaml --create-namespace -n ingress-nginx

# Create a ClusterIssuer resource for cert-manager
cat <<EOF > gateway-class.yml
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: test-mail@gmail.com
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class: nginx
EOF

# Apply the ClusterIssuer resource
kubectl apply -f gateway-class.yml
```

### ⚙️ Deploying Grafana and Prometheus

Grafana and prometheus are added in ArgoCD as an applicationSet, all changes that are in monitoring stack repo will be synced automatically in cluster. Please run the following commands to deploy them:

```bash
# Create a Kubernetes YAML file 'appset.yml' and specify its content
cat <<EOF > appset.yml
---
apiVersion: argoproj.io/v1alpha1
kind: ApplicationSet
metadata:
  name: bootstrap-tools
  namespace: argocd
spec:
  generators:
  - git:
      repoURL: https://gitlab.com/Aminechakr/evmos.git
      revision: main
      directories:
      - path: bootstrap/*
      # - exclude: true
      #   path: tools/cert-manager
  template:
    metadata:
      name: '{{path.basename}}'
    spec:
      project: default
      source:
        repoURL: https://gitlab.com/Aminechakr/evmos-project.git
        targetRevision: main
        path: '{{path}}'
        helm:
          valueFiles:
            - values.yaml
            - dev.yaml
      destination:
        server: https://kubernetes.default.svc
        namespace: '{{path.basename}}'
      syncPolicy:
        syncOptions:
          - CreateNamespace=true
EOF

# Apply the Kubernetes resource configuration from 'appset.yml'
kubectl apply -f appset.yml
```

### ⚙️ Deploying Evmos

To deploy the custom application, Evmos (evmosd), and ensure its proper monitoring, follow these steps:

1. Preparing evmos variables and parameters to lunch testnet node
```yaml
command: 'evmosd init "evmos-testnet-challenge" --chain-id="evmos_9000-4" --home="/home/evmos/.tmp-evmosd" && \
           wget -O /home/evmos/.tmp-evmosd/config/genesis.zip https://github.com/evmos/testnets/raw/main/evmos_9000-4/genesis.zip && \
           unzip -o /home/evmos/.tmp-evmosd/config/genesis.zip -d /home/evmos/.tmp-evmosd/config/ && \
           evmosd start --log_level="info" --minimum-gas-prices=0.0001aevmos --json-rpc.api=eth,txpool,personal,net,debug,web3 --home="/home/evmos/.tmp-evmosd" --chain-id="evmos_9000-4"'
```
2. The helm chart is produced for Evmos usage, the ``dev.yml`` and ``values.yml`` take in consideration all application parameters. 

3. ArgoCD is taking in charge syncing ``Evmosd`` automatically across the cluster using ``ApplicationSet`` that was shared in monitoring stack previousely using the defined specs.

## 🔄 Continuous Integration

The Continuous Integration (CI) pipeline automates various tasks to ensure the reliability and security of your project. This section provides an overview of the CI stages and their respective jobs.

### 📦 Stages

The CI pipeline is organized into the following stages:

- **Test**: This stage is responsible for running tests to ensure code correctness and functionality.

- **Scan**: In the Scan stage, various security analyzers are employed to scan your project for vulnerabilities and issues.

- **Release**: The Release stage handles the building and deployment of project artifacts, including Docker images.

### 🧪 Test Stage

The Test stage encompasses tasks related to testing your project. It ensures that the code functions correctly and meets the required quality standards. Tests include unit tests, integration tests, and more.

In this stage, a series of tests are executed to verify the correctness of your project's code. The following tests are included:

- **Static Analysis**: This step includes running the `go fmt`, `go vet`, and `go test` commands on your code. These commands check for code formatting, potential issues, and run race condition tests.

Here's an overview of the `go test` job in this stage:

- **Image**: `golang:1.21-alpine3.18`
- **Variables**:
  - `GIT_REPO`: The GitLab repository where your project is hosted.
  - `SAST_GOSEC_LEVEL`: Security level for GoSec analysis.
  - `SAST_DEFAULT_ANALYZERS`: Default analyzers for security scanning.
- **Script**:
  - Installation of necessary dependencies.
  - Displaying the GitLab build reference and project directory.
  - Checking the Go version.
  - Preparing your project directory for testing.
  - Running static analysis, including formatting checks, code vetting, and race condition tests.

The Test stage ensures that your project's code is correct and adheres to quality standards.

The CI pipeline runs automatically whenever changes are made to your project, guaranteeing that code changes are thoroughly tested and analyzed.

For more details on the Test stage and individual jobs, refer to the [`.test-ci.yml`](.test-ci.yml) file in your project repository.

### 🔍 Scan Stage

In the Scan stage, the CI pipeline focuses on security scanning. It employs various security analyzers to identify vulnerabilities and potential issues in your code. One of the scanners used in this stage is [gosec](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html), a tool for analyzing Go source code.

Here's an overview of the GoSec job in this stage:

- **Job**: `gosec`
- **Image**: `$SAST_ANALYZER_IMAGE`
- **Analyzer Image Tag**: `3`
- **Analyzer Image**: `$SECURE_ANALYZERS_PREFIX/gosec:$SAST_ANALYZER_IMAGE_TAG`
- **Artifacts**: The GoSec job produces a SAST report named `gl-sast-report.json`.

The `gosec` job helps identify security vulnerabilities in your Go codebase.

### 🚢 Release Stage

The Release stage is responsible for building and deploying project artifacts, which include Docker images. The deployment process ensures that your project is ready for distribution or deployment to production environments.

The job in this stage, which builds and pushes Docker images, is not detailed here but is a crucial part of the release process.

The CI release jobs run an update version after script that makes sure new release is updated on the helm chart, thus ArgoCD take in charge all new changes and sync it over the cluster or clusters. 

For more details on the CI pipeline and individual jobs, refer to the [`.gitlab-ci.yml`](.gitlab-ci.yml) file in your project repository.

## 🕵️ Troubleshooting

There has been a lot of troubleshooting that we unfortunately couldn't keep track of and document properly due to time constraints.

## 📚 References

Here are the resources and references used in this project for further information:

1. **Evmos Dockerfile:** [GitHub - Dockerfile](https://github.com/evmos/evmos/blob/main/Dockerfile)

2. **Evmos Official Documentation:** [Evmos Docs](https://docs.evmos.org/)

3. **GKE Official Documentation:** [Google Cloud Kubernetes Engine Documentation](https://cloud.google.com/kubernetes-engine)

4. **Sample GKE clusters Infrastructure as Code (IAC) with Bastion Host:** [GitHub - Terraform-Google-Kubernetes-Engine](https://github.com/terraform-google-modules/terraform-google-kubernetes-engine/tree/master/examples/)

5. **Deploying multi-cluster Gateways:** [Official GCP article](https://cloud.google.com/kubernetes-engine/docs/how-to/deploying-multi-cluster-gateways)

6. **Building multiple interconnected GKE Clusters:** [PingCAP Guide](https://docs.pingcap.com/tidb-in-kubernetes/stable/build-multi-gcp-gke)

7. **GKE Terraform Registry:** [Terraform Google Provider Documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/using_gke_with_terraform)

8. **ArgoCD Official Documentation:** [ArgoCD Documentation](https://argo-cd.readthedocs.io/en/stable/)

9. **ArgoCD Helm Values File:** [GitHub - Argo-Helm](https://github.com/argoproj/argo-helm/blob/main/charts/argo-cd/values.yaml)

These references provide additional information and details related to the tools, architecture, and resources used in this project.
