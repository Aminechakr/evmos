######## Expand the name of the chart.########

{{- define "evmos.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "evmos.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


######## Create chart name and version as used by the chart label.########

{{- define "evmos.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}




######## Common labels ########

{{- define "evmos.labels" -}}
helm.sh/chart:  {{ include "evmos.chart" . }}
{{ include "evmos.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}



######## Selector labels ########

{{- define "evmos.selectorLabels" -}}
app.kubernetes.io/name:     {{ include "evmos.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}



########
{{- define "labels" -}}
app:          {{ .Values.appName }}
environment:  {{ .Values.environment }}
tier:         {{ .Values.tier }}
{{- end -}}
########


### Create the name of the service account to use ###

{{- define "evmos.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "evmos.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}





########
{{- define "preStop-duration" -}}
{{- if .Values.preStopDuration.preStopDurationEnabled }}
    {{- if .Values.preStopDuration.duration -}}
        {{- printf "%s" .Values.preStopDuration.duration -}}
    {{- else -}}
        {{- printf "10" }}
    {{- end -}}
{{- end -}}
{{- end -}}
########



########
{{- define "AutoRollingUpdate" -}}
{{ .Values.autoRollingUpdateEnabled | default "true" }}
{{- end -}}
########




########
{{- define "fullPath-registry" -}}
{{- printf "%s/%s:%s" .Values.containerImage.repository .Values.containerImage.imageName .Values.containerImage.appVersion  -}}
{{- end -}}
########




########
{{- define "containerSecurity" -}}
runAsUser:    {{ .Values.containerSecurity.runAsUser }}
runAsGroup:   {{ .Values.containerSecurity.runAsGroup }}
runAsNonRoot: {{ .Values.containerSecurity.runAsNonRoot }}
{{- end -}}
########



########
{{- define "rollingStrategy" -}}
{{- if .Values.strategy.strategyEnabled -}}
type: {{ .Values.strategy.type }}
rollingUpdate:
   maxUnavailable:  {{ .Values.strategy.rollingUpdate.maxUnavailable }}
   maxSurge:        {{ .Values.strategy.rollingUpdate.maxSurge }}
{{- end -}}
{{- end -}}
########



########
{{- define "probes" -}}
{{- if .Values.readinessProbe.enableReadinessProbe }}
readinessProbe:
  httpGet:
    path:   {{ .Values.readinessProbe.httpGetPath }}
    port:   {{ .Values.containerPort }}
    scheme: {{ .Values.readinessProbe.httpGetScheme }}
  initialDelaySeconds:  {{ .Values.readinessProbe.initialDelaySeconds }}
  timeoutSeconds:       {{ .Values.readinessProbe.timeoutSeconds }}
  periodSeconds:        {{ .Values.readinessProbe.periodSeconds }}
  successThreshold:     {{ .Values.readinessProbe.successThreshold }}
  failureThreshold:     {{ .Values.readinessProbe.failureThreshold }}
{{- end }}
{{- if .Values.livenessProbe.enableLivenessProbe }}
livenessProbe:
  httpGet:
    path:   {{ .Values.livenessProbe.httpGetPath }}
    port:   {{ .Values.containerPort }}
    scheme: {{ .Values.livenessProbe.httpGetScheme }}
  initialDelaySeconds:  {{ .Values.livenessProbe.initialDelaySeconds }}
  timeoutSeconds:       {{ .Values.livenessProbe.timeoutSeconds }}
  periodSeconds:        {{ .Values.livenessProbe.periodSeconds }}
  successThreshold:     {{ .Values.livenessProbe.successThreshold }}
  failureThreshold:     {{ .Values.livenessProbe.failureThreshold }}
{{- end }}
{{- if .Values.startupProbe.enableStartupProbe }}
startupProbe:
  httpGet:
    path:   {{ .Values.startupProbe.httpGetPath }}
    port:   {{ .Values.containerPort }}
    scheme: {{ .Values.startupProbe.httpGetScheme }}
  initialDelaySeconds:  {{ .Values.startupProbe.initialDelaySeconds }}
  timeoutSeconds:       {{ .Values.startupProbe.timeoutSeconds }}
  periodSeconds:        {{ .Values.startupProbe.periodSeconds }}
  successThreshold:     {{ .Values.startupProbe.successThreshold }}
  failureThreshold:     {{ .Values.startupProbe.failureThreshold }}
{{- end }}
{{- end }}
#########


{{/*
{{- $path := include "filePath" (dict "env" "dev" "filename" "application.properties" "Files" $.Files) }}
*/}}

{{- define "filePath" -}}
    {{- $envFile := .Files.Get (printf "config/%s/%s" .env .filename) -}}
    {{- if $envFile -}}
        {{- printf "config/%s/%s" .env .filename -}}
    {{- else -}}
        {{- printf "resources/%s" .filename }}{{- end -}}
{{- end -}}